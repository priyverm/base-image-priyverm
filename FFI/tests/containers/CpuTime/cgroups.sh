#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

cleanup() {
  echo $$ > $cgroups_base/cgroup.procs
  test -f $tmptest          && rm $tmptest
  test -d $cgroup_confusion && rmdir $cgroup_confusion
  test -d $cgroup_orderly   && rmdir $cgroup_orderly
}

trap "cleanup" 0 

if test -z "$ORDERLY_CGROUP_VAL" && test -z "$CONFUSION_CGROUP_VAL" || \
   test -z "$CGROUP_CTRL"; then
  error "No cgroups control given!"
  exit 1
fi

cgroups_base=$(mount | grep cgroup2 | cut -d ' ' -f 3)
if test -z "$cgroups_base"; then
  error "cgroupsv2 not enabled!"
  exit 1
fi

echo "+cpu" > $cgroups_base/cgroup.subtree_control

cgroup_orderly=$(mktemp --tmpdir=$cgroups_base --directory orderly.XXXXXX)
cgroup_confusion=$(mktemp --tmpdir=$cgroups_base --directory confusion.XXXXXX)

if test ! -d $cgroup_orderly || test ! -d $cgroup_confusion; then
  error "Failed creating cgroups!"
  exit 2
fi

echo "+cpu" > $cgroup_orderly/cgroup.subtree_control
echo "+cpu" > $cgroup_confusion/cgroup.subtree_control

test -n "$ORDERLY_CGROUP_VAL"   && echo "$ORDERLY_CGROUP_VAL"   > $cgroup_orderly/$CGROUP_CTRL
test -n "$CONFUSION_CGROUP_VAL" && echo "$CONFUSION_CGROUP_VAL" > $cgroup_confusion/$CGROUP_CTRL

# This is uglyyy....
echo $$ > $cgroup_orderly/cgroup.procs

tmptest=$(mktemp $(dirname $(readlink -f $0))/test.XXXXXXX)
sed "/^podman run.*--name confusion/aecho \$! > $cgroup_confusion/cgroup.procs
     /^podman run.*--name confusion/aecho 'Adding confusion to cgroup'" test.sh > $tmptest
/bin/bash $tmptest --container-opts "--cgroups=disabled" "$@"

# This does not work..
#/bin/bash ./test.sh --orderly-opts   "--cgroupns=host" \
#                    --orderly-opts   "--cgroup-parent=$cgroup_orderly" \
#                    --confusion-opts "--cgroupns=host" \
#                    --confusion-opts "--cgroup-parent=$cgroup_confusion" \
#                    "$@"
ret=$?

exit $ret

